package id.amaliafiqhiyah.amalia_1202164314_si40int_pab_modul3;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class RecyclerViewAdapter extends RecyclerView.Adapter<RecyclerViewAdapter.ViewHolder> {

    public List<Jobs> jobs;
    private Activity activity;
    Context context;

    public RecyclerViewAdapter(Activity activity, List<Jobs> jobs) {
        this.activity = activity;
        this.jobs = jobs;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.list_item, viewGroup, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {
        final String nama = jobs.get(position).getName();
        final String pekerjaan = jobs.get(position).getJob();
        final int image = jobs.get(position).getAvatar();

        viewHolder.name.setText(jobs.get(position).getName());
        viewHolder.job.setText(jobs.get(position).getJob());

        if (jobs.get(position).isGender()) {
            viewHolder.imageView.setImageResource(R.mipmap.male);
        } else {
            viewHolder.imageView.setImageResource(R.mipmap.female);
        }

        viewHolder.setItemClickListener(new ItemClickListener() {
            @Override
            public void onItemClick(int pos) {
                openDetailActivity(nama, pekerjaan, image);
            }
        });
    }

    private void setDataToView(TextView name, TextView job, ImageView genderIcon, int position) {
        name.setText(jobs.get(position).getName());
        job.setText(jobs.get(position).getJob());
        if (jobs.get(position).isGender()) {
            genderIcon.setImageResource(R.mipmap.male);
        } else {
            genderIcon.setImageResource(R.mipmap.female);
        }
    }

    @Override
    public int getItemCount() {
        return (null != jobs ? jobs.size() : 0);
    }

    private void openDetailActivity(String name, String pekerjaan, int gambar) {
        Intent i = new Intent(activity.getApplicationContext(), DetailActivity.class);
        i.putExtra("nama", name);
        i.putExtra("pekerjaan", pekerjaan);
        i.putExtra("gambar", gambar);
        activity.startActivity(i);
    }

    private View.OnClickListener onClickListener(final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog = new Dialog(activity);
                dialog.setContentView(R.layout.list_item);
                dialog.setTitle("Position " + position);
                dialog.setCancelable(true);

                TextView name = (TextView) dialog.findViewById(R.id.name);
                TextView job = (TextView) dialog.findViewById(R.id.job);
                ImageView icon = (ImageView) dialog.findViewById(R.id.image);

                setDataToView(name, job, icon, position);

                dialog.show();
            }
        };
    }

    protected class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private ImageView imageView;
        private TextView name;
        private TextView job;
        private View container;
        ItemClickListener itemClickListener;

        public ViewHolder(View itemView) {
            super(itemView);
            imageView = (ImageView) itemView.findViewById(R.id.image);
            name = (TextView) itemView.findViewById(R.id.name);
            job = (TextView) itemView.findViewById(R.id.job);
            container = itemView.findViewById(R.id.card_View);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            this.itemClickListener.onItemClick(this.getLayoutPosition());
        }

        public void setItemClickListener(ItemClickListener itemClickListener) {
            this.itemClickListener = itemClickListener;
        }
    }
}
