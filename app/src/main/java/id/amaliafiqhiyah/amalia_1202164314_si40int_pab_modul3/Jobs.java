package id.amaliafiqhiyah.amalia_1202164314_si40int_pab_modul3;

public class Jobs {
    private String name;
    private String job;
    private int avatar;
    private boolean gender;

    public Jobs(String name, boolean gender, String job, int avatar) {
        this.name = name;
        this.gender = gender;
        this.job = job;
        this.avatar = avatar;
    }

    public String getName() {
        return name;
    }

    public String getJob() {
        return job;
    }

    public int getAvatar() {
        return avatar;
    }

    public boolean isGender() {
        return gender;
    }
}
