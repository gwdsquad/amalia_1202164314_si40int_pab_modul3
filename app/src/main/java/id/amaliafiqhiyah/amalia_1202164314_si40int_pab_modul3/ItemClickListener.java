package id.amaliafiqhiyah.amalia_1202164314_si40int_pab_modul3;

public interface ItemClickListener {
    void onItemClick(int pos);
}
