package id.amaliafiqhiyah.amalia_1202164314_si40int_pab_modul3;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;


public class DetailActivity extends AppCompatActivity {

    ImageView ivIcon;
    TextView tvName, tvJob;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        ivIcon = (ImageView) findViewById(R.id.gender_icon);
        tvName = (TextView) findViewById(R.id.name_2);
        tvJob = (TextView) findViewById(R.id.job_2);

        Intent i = this.getIntent();
        String nama = i.getExtras().getString("nama");
        String pekerjaan = i.getExtras().getString("pekerjaan");
        int gambar = i.getExtras().getInt("gambar");

        tvName.setText(nama);
        tvJob.setText(pekerjaan);
        ivIcon.setImageResource(gambar);
    }
}
